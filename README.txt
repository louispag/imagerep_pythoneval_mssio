Introduction: 

Le projet consiste en une API locale qui stocke des images.
Seules les images JPEG sont acceptées. 
Les métadata sont extraites des images avant qu'elles soient transformées en vignettes et
stockées sur le système.
La taille des vignettes peut être changée dans le fichier CONST.py
l'API propose trois endpoints:
-post d'une image, le serveur renvoie une ID : http://127.0.0.1/images
-get d'une image afin de récupérer son nom, son lien de téléchargement
ainsi que ses métadata : http://127.0.0.1/images/<id>
-téléchargement d'une vignette : http://127.0.0.1/thumbnails/id

Télécharger le projet:
https://gitlab.com/louispag/imagerep_pythoneval_mssio.git
ou
git clone https://gitlab.com/louispag/imagerep_pythoneval_mssio.git

Technologies et leur version utilisée dans le projet:

Python 3.8.5
Flask==1.1.2
Flask-RESTful==0.3.8
Pillow==7.0.0
piexif==1.1.3
simplejson==3.16.0
requests==2.22.0

Répertoires et fichiers:

Le répertoire du projet est constitué de 4 fichiers python:
CONST.py : Il contient les constantes du projet. 
my_methods.py : Il contient les différentes fonctions utilisées dans le projet.
unitestAPI.py : Il contient le script des tests unitaires. 
flaskapp.py : Il contient le script de l'API Flask. 

Le répertoire du projet est constitué de 2 dossiers:
repository : dossier dans lequel sont stockées les vignettes.
testFiles : dossier dans lequel sont stockées les images utilisées pour les tests unitaires.

Installation : 

Par défaut l'adresse de connexion de l'API est : 127.0.0.1
Port exposé par défaut : 5000
Ces deux valeurs peuvent être modifiées dans le fichier CONST.py
Placer les fichiers du projet n'importe ou sur la machine hôte.
le current working directory est chargé. S'il y a le moindre problème à l'exécution du
projet, mettre à jour la variable CURRENT_DIRECTORY dans le fichier CONST.py avec le chemin du
projet actuel. 
S'assurer de la présense de tous les fichiers python et des deux répertoires dans le dossier.
S'ils n'y sont pas, les créer en respectant bien les noms mentionnés dans la partie "Répertoires
et fichiers", sinon les changer dans le fichier CONST.py

Se placer dans le dossier du projet et lancer l'application avec:
	$ python3 flaskapp.py
lancer les tests unitaires avec:
	$ python3 unitestAPI.py
	
Exemple d'utilisation de l'API dans du code python avec le module requests:

prérequis : import requests 

Poster une image:
	img = {"image" : open(<IMAGE_PATH> + "<IMAGE_NAME>", 'rb')}
	r=requests.post('http://127.0.0.1:5000' + "/images", files = img)
Récupérer les métadata d'une image:
	r=requests.get('http://127.0.0.1:5000' + "/images/<ID>")
Télécharger une vignette:
	r=requests.get('http://127.0.0.1:5000' + "/thumbnails/<ID>")
	
	
	
	
	
	
	
	
	
	
