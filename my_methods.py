#
#   Python Project 
#
#Image API rep project
#Louis Paganin Mastère Spé SIO CentraleSupélec
#
#my_methods script

import os
import CONST
import re
import json
from PIL.ExifTags import TAGS
from PIL import Image

#return True if the image is in the repository, if not return False
def isImage(imageID):
    files = os.listdir(CONST.REPO_PATH)
    for file in files:
        idf, e = os.path.splitext(file)
        if imageID == int(idf):
            return True
    return False

#return image meta data in json format
def getMetaData(imageName):
    im = Image.open(CONST.REPO_PATH + imageName)
    metadata = im._getexif()
    if metadata != None:
        metadata = get_labeled_exif(metadata)
        return metadata
    else: 
        return "no metadata" 

#return the first availiable ID for an image as a string
#gérer chiffre dans nombre
def getAvaiID():
    files = os.listdir(CONST.REPO_PATH)
    ID = 1
    while(True):
        isIdPresent = 0
        ##probleme avec le point
        for file in files:
            idf, e = os.path.splitext(file)
            if ID == int(idf):
                isIdPresent = 1
                break
        if isIdPresent == 0:
            return str(ID)
        ID = ID + 1

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in CONST.ALLOWED_EXTENSIONS

def get_labeled_exif(exif):
    labeled = {}
    for (key, val) in exif.items():
        labeled[TAGS.get(key)] = val

    return labeled