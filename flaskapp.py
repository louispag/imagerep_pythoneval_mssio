#
#   Python Project 
#
#Image API rep project
#Louis Paganin Mastère Spé SIO CentraleSupélec
#
#app script

from flask import Flask, request, send_from_directory, jsonify
from flask_restful import Api, Resource, abort
import my_methods
import CONST
from PIL import Image
import piexif
import os

#create an instance of the class
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = CONST.REPO_PATH
api = Api(app)

class images(Resource):
    #when called, returns State, metadata, link to the thumbnail
    def get(self, imageID):
        if imageID == ' ':
            return {"error": "no ID"}
        if my_methods.isImage(imageID):
            imageName = str(imageID) + ".jpeg"
            return jsonify({"Name": imageName,
                "link" : "http://" + CONST.ADRESS + "/thumbnails/" + str(imageID),
                "metadata" : my_methods.getMetaData(str(imageID) + ".jpeg")}
            )
        else:    
            return {"error": "no image with the ID:" + str(imageID)}

    def post(self):  
        if 'image' not in request.files:
            return {"error": "no image part"}
        img = request.files['image']
        if not my_methods.allowed_file(img.filename):
            return {"error" : "the format is not correct"}
        if img.filename == '':
            return {"error": "no selected image"}
        if img and my_methods.allowed_file(img.filename):            
            img = Image.open(img)
            img.thumbnail(CONST.SIZE_THUMBNAILS)
            ID = my_methods.getAvaiID()
            #if metadat is not None, save the image with its metadata
            if img._getexif() != None:
                exif_dict = piexif.load(img.info["exif"])
                exif_bytes = piexif.dump(exif_dict)
                img.save(os.path.join(app.config['UPLOAD_FOLDER'], ID + ".jpeg"), exif=exif_bytes)
            else:
                img.save(os.path.join(app.config['UPLOAD_FOLDER'], ID + ".jpeg"))
            return {"id" : ID}

class thumbnails(Resource):
    def get(self, imageID):
        if my_methods.isImage(imageID):
            return send_from_directory(app.config['UPLOAD_FOLDER'],str(imageID) + ".jpeg", as_attachment=True)
        else:
            return {"error" : "no image with the ID : " + str(imageID)}
    
api.add_resource(images, "/images/<int:imageID>", "/images")
api.add_resource(thumbnails, "/thumbnails/<int:imageID>")

#run the app
if __name__ == '__main__':
    #CONST.CURRENT_DIRECTORY = os.getcwd()
    print("the current path working directory is : " + CONST.CURRENT_DIRECTORY)
    app.run(host=CONST.ADRESS, port=CONST.PORT)
